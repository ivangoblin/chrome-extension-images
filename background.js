var executedExtension = [];
chrome.browserAction.onClicked.addListener(function(tab) {
    var alreadyDefined = executedExtension.some(function(item){item === tab.id});
    if(!alreadyDefined) {
        chrome.tabs.executeScript(tab.id, {
            file: 'imageExtractor.js'
        });
        executedExtension.push(tab.id);
    }
    
});

var images = [];

chrome.runtime.onMessage.addListener(
  function(request, sender, sendResponse) {
    if (request['images']) {
        images = request["images"];
        chrome.tabs.create({url: "/images.html"});
    }
      
      if(request === "getData") {
          sendResponse({images: images});
      }
  });
