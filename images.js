
chrome.runtime.sendMessage("getData", function(response) {
    var imagesContainer = document.getElementById("images");
    var images = response.images;
    console.log(images);
    for(var i=0, l=images.length;i < l; i++) {
        var image = document.createElement("img");
        image.setAttribute("src", images[i]);
        imagesContainer.appendChild(image);
    }
})